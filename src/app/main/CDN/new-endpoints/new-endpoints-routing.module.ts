import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewEndpointsComponent } from './new-endpoints.component';

const routes: Routes = [
  {
    path: '', component:NewEndpointsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewEndpointsRoutingModule { }
