import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, NgForm, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isInvalid = false;
  loginForm: FormGroup;
  invalidMsg = 'Invalid Email or Password';
  @ViewChild('formDir') private formDir: NgForm;

  constructor(private fb: FormBuilder, private router: Router, private auth: AuthService, private ad: ActivatedRoute) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  logIn() {



    const formData = new FormData();
    if (this.loginForm.valid) {
      formData.append('email', this.loginForm.value.email);
      formData.append('password', this.loginForm.value.password);
      
      this.auth.loggedIn(formData).subscribe((res: any) => {
     
        if (res.code === 1) {
          localStorage.setItem('newUser', JSON.stringify(res.result));
          this.router.navigate(['dashboard']);

        } else {
          this.isInvalid = true;

        }
        this.loginForm.reset();
        this.formDir.resetForm();
      })
    }
  }

}


