import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [];

export const LayoutRoutes: Routes = [
  { path: 'dashboard', loadChildren: () => import('../main/CDN/dashboard/dashboard.module').then(m => m.DashboardModule) },


  { path: 'new-endpoints', loadChildren: () => import('../main/CDN/new-endpoints/new-endpoints.module').then(m => m.NewEndpointsModule) },


  { path: 'endpoints', loadChildren: () => import('../main/CDN/endpoints/endpoints.module').then(m => m.EndpointsModule) },
  
  
  { path: 'vod', loadChildren: () => import('../main/File_Transcoding/vod/vod.module').then(m => m.VodModule) },


  { path: 'vod-bulk', loadChildren: () => import('../main/File_Transcoding/vod-bulk/vod-bulk.module').then(m => m.VodBulkModule) },


  { path: 'audio', loadChildren: () => import('../main/File_Transcoding/audio/audio.module').then(m => m.AudioModule) },
  
  
  { path: 'live-transcoding', loadChildren: () => import('../main/live-transcoding/live-transcoding.module').then(m => m.LiveTranscodingModule) },


  { path: 'settings', loadChildren: () => import('../main/settings/settings.module').then(m => m.SettingsModule) },

];
