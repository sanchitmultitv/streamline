import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LiveTranscodingComponent } from './live-transcoding.component';

const routes: Routes = [
  {
    path: '', component: LiveTranscodingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LiveTranscodingRoutingModule { }
