import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LiveTranscodingRoutingModule } from './live-transcoding-routing.module';
import { LiveTranscodingComponent } from './live-transcoding.component';


@NgModule({
  declarations: [
    LiveTranscodingComponent
  ],
  imports: [
    CommonModule,
    LiveTranscodingRoutingModule
  ]
})
export class LiveTranscodingModule { }
