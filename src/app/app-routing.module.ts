import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'login', loadChildren: ()=>import('./login/login.module').then(m=>m.LoginModule)},
  {
    path: '',
    component: LayoutComponent,
    children: [
        { path: '', loadChildren: ()=>import('./layout/layout.module').then(m=>m.LayoutModule)}
      ],canActivate:[AuthGuard],
  },
  // {path: '**', redirectTo: '/'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
