import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VodBulkRoutingModule } from './vod-bulk-routing.module';
import { VodBulkComponent } from './vod-bulk.component';


@NgModule({
  declarations: [
    VodBulkComponent
  ],
  imports: [
    CommonModule,
    VodBulkRoutingModule
  ]
})
export class VodBulkModule { }
