import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewEndpointsRoutingModule } from './new-endpoints-routing.module';
import { NewEndpointsComponent } from './new-endpoints.component';


@NgModule({
  declarations: [
    NewEndpointsComponent
  ],
  imports: [
    CommonModule,
    NewEndpointsRoutingModule
  ]
})
export class NewEndpointsModule { }
