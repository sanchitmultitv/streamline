import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {


  }





  loggedIn(par) {

    return this.http.post<any>(`${this.baseUrl}/author_backend/v1/auth/login`, par)
  

  };


  logout() {
    
    localStorage.removeItem('newUser');

  }





}

