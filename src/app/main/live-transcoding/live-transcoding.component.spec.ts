import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveTranscodingComponent } from './live-transcoding.component';

describe('LiveTranscodingComponent', () => {
  let component: LiveTranscodingComponent;
  let fixture: ComponentFixture<LiveTranscodingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveTranscodingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveTranscodingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
